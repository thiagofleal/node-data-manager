export class PromiseQueue {
  private queue: Promise<unknown> = Promise.resolve();

  async push<T>(operation: () => Promise<T>): Promise<T> {
    return new Promise<T>((resolve, reject) => {
      this.queue = this.queue
        .then(operation)
        .then(resolve)
        .catch(reject)
    })
  }
}
