export function factory<T>(
  value: T, ...args: T extends (...args: any[]) => any ? Parameters<T> : never
): T extends (...args: any[]) => any ? ReturnType<T> : T {
  return typeof value === "function" ? value(...args) : value;
}
