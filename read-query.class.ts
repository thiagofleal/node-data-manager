import { Readable, Transform } from "node:stream";

import { Collection } from "./collection.class";

export class ReadQuery<T> {
  constructor(
    private stream: Readable
  ) { }

  setStream(stream: Readable) {
    this.stream = stream;
  }

  where(condition: (data: T) => boolean): this {
    this.setStream(this.stream.filter(condition));
    return this;
  }

  select<U>(transform: (data: T) => U): ReadQuery<U> {
    return new ReadQuery(this.stream.map(transform));
  }

  take(count: number): this {
    let i = 0;
    this.setStream(
      this.stream.pipe(
        new Transform({
          objectMode: true,
          transform(chunk, _, callback) {
            if (i++ < count) {
              callback(null, chunk);
            } else {
              this.push(null);
            }
          }
        })
      )
    );
    return this;
  }

  joinMany<U, V = U>(
    field: keyof T,
    collection: Collection<U>,
    condition: (dest: T, src: U) => boolean,
    opts?: {
      transform?: (data: U[]) => V | Promise<V>,
      setQuery?:  (query: ReadQuery<U>) => ReadQuery<U>
    }
  ): this {
    this.setStream(
      this.stream.pipe(
        new Transform({
          objectMode: true,
          transform(chunk, _, callback) {
            const row = chunk as T | null;

            if (row) {
              let reader: ReadQuery<U> | ReadQuery<V> = collection.reader()
                .where(data => condition(row, data));

              if (opts?.setQuery) {
                reader = opts.setQuery(reader);
              }
              reader.read()
                .then(async data => {
                  let value = data as U[] | V;

                  if (opts?.transform) {
                    value = await opts.transform(data);
                  }
                  row[field] = value as typeof row[keyof T];
                  callback(null, row);
                });
            } else {
              this.push(null);
            }
          }
        })
      )
    );
    return this;
  }

  join<U, V = U>(
    field: keyof T,
    collection: Collection<U>,
    condition: (dest: T, src: U) => boolean,
    opts?: {
      transform?: (data: U | null) => V | Promise<V>,
      setQuery?:  (query: ReadQuery<U>) => ReadQuery<U>
    }
  ): this {
    return this.joinMany(field, collection, condition, {
      setQuery: query => {
        query = query.take(1);

        if (opts?.setQuery) {
          query = opts.setQuery(query);
        }
        return query;
      },
      transform: async data => {
        let value = (data.at(0) ?? null) as V | null;

        if (opts?.transform) {
          value = await opts.transform(value as U | null);
        }
        return value;
      }
    });
  }

  pipe(
    destination: NodeJS.WritableStream,
    options?: { end?: boolean | undefined }
  ): NodeJS.WritableStream {
    return this.stream.pipe(destination, options);
  }

  async read(): Promise<T[]> {
    return this.stream.toArray();
  }
}
