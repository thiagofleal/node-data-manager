import { existsSync, mkdirSync } from "node:fs";
import { join } from "node:path";

import { Collection } from "./collection.class";


export class DataManager {
  constructor(
    private path: string
  ) {
    if (!existsSync(path)) {
      mkdirSync(path, { recursive: true });
    }
  }

  collection<T>(name: string): Collection<T> {
    return new Collection(join(this.path, name));
  }
}
