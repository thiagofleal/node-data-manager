import { ReadStream, createReadStream, createWriteStream } from "node:fs";
import { EOL } from "node:os";
import { rename } from "node:fs/promises";
import { createInterface } from "node:readline";

import { ReadQuery } from "./read-query.class";
import { factory } from "./utils/functions";
import { PromiseQueue } from "./utils/promise-queue.class";


export class Collection<T = any> {
  private static queues: Record<string, PromiseQueue> = {};

  private get queue(): PromiseQueue {
    return Collection.queues[this.path] ??= new PromiseQueue();
  }

  constructor(
    private path: string
  ) {}

  reader(): ReadQuery<T> {
    const rl = createInterface({
      input: createReadStream(this.path, { encoding: "utf8", flags: "a+" }),
      crlfDelay: Infinity
    });
    return new ReadQuery(ReadStream.from(rl).map(e => JSON.parse(e)));
  }

  async insert(value: T): Promise<void> {
    return this.queue.push(() => new Promise<void>((resolve, reject) => {
      try {
        const stream = createWriteStream(this.path, { encoding: "utf8", flags: "a" });

        stream.on("ready", () => {
          stream.write(JSON.stringify(value) + EOL, error => {
            if (error) {
              reject(error);
            } else {
              stream.close(error => {
                error ? reject(error) : resolve();
              });
            }
          });
        });
      } catch (e) {
        reject(e);
      }
    }));
  }

  private async replace(
    where: (value: T) => boolean,
    value: T | ((e: T) => T) | null
  ): Promise<number> {
    return this.queue.push(() => new Promise<number>((resolve, reject) => {
      let count = 0;

      try {
        const filename = this.path + ".tmp";
        const stream = createWriteStream(filename, { encoding: "utf8", flags: "a" });

        stream.on("ready", async () => {
          const rows = await this.reader().read();

          await Promise.all(rows.map(async row => {
            if (where(row)) {
              count++;
              if (value)  row = factory(value, row);
              else return;
            }
            await new Promise<void>(r => {
              stream.write(JSON.stringify(row) + EOL, error => {
                if (error)  reject(error);
                else r();
              });
            });
          }));
          stream.close(async error => {
            const save = (): Promise<void> => rename(filename, this.path)
              .catch(save);

            await save();
            error ? reject(error) : resolve(count);
          });
        });
      } catch (e) {
        reject(e);
      }
    }));
  }

  async update(value: T | ((e: T) => T), where: (value: T) => boolean): Promise<number> {
    return this.replace(where, value);
  }

  async remove(where: (value: T) => boolean): Promise<number> {
    return this.replace(where, null);
  }
}
